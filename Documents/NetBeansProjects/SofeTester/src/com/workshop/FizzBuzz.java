
package com.workshop;

/**
 *
 * @author meny2
 */
public class FizzBuzz {
    
    public static String div(int x){
        if (x%3 == 0 && x%5 == 0) {
            return "FizzBuzz";
        }else if(x%3 == 0){
            return "fizz";
        }else if(x%5 == 0){
            return "buzz";
        }
        return "";
    }
    
}
