
package com.workshop;

/**
 *
 * @author meny2
 */
public class StringOperations {
    
    public static String reverse(String s){
        StringBuilder b = new StringBuilder();
        for (int i = s.length()-1; i >= 0; i--) {
            b.append(s.charAt(i));
        }
        return b.toString();
    }
    
}
