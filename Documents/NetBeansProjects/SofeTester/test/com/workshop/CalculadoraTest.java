package com.workshop;

import static org.hamcrest.CoreMatchers.is;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;

/**
 *
 * @author meny2
 */
public class CalculadoraTest {

    @Test
    public void addTest() {
        assertEquals(12, Calculadora.add(4, 8));
    }

    @Test
    public void substractTest() {
        assertThat(10, is(Calculadora.substract(15, 5)));
    }
}
