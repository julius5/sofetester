
package com.workshop;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author meny2
 */
public class FizzBuzzTest {
    
    @Test
    public void fizzBuzzDiv(){
        assertEquals("fizz", FizzBuzz.div(9));
        assertEquals("buzz", FizzBuzz.div(10));
        assertEquals("FizzBuzz", FizzBuzz.div(30));
        assertEquals("", FizzBuzz.div(2));
    }
}
