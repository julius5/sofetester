
package com.workshop;

import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author meny2
 */
public class RevertStringTest {
    
   @Test
   public void revertString(){
      List Strings = new ArrayList<String>();
      Strings.add("cba");
      Strings.add("shdshfsa");
      Strings.add("lolo");
      
      List results = new ArrayList<String>();
      results.add("abc");
      results.add("asfhsdhs");
      results.add("olol");
      
       for (int i = 0; i < Strings.size(); i++) {
           assertEquals(results.get(i),StringOperations.reverse((String) Strings.get(i)));
       }
   }
   
}
